
# Image Processing Using MATLAB (GUI)

Implementation of basic Image Processing techniques using the Image Processing Toolbox.



## Author

- [@UnnatMistry](https://www.github.com/UnnatMistry)


## Features

- RGB to Gray
- Image to Binary Image
- Plot Histogram of Image
- Complement Image
- Edge Detection
- Rotate image


## Screenshot

![App Screenshot](https://bitbucket.org/unnatmistry/basic-image-processing-using-matlab-gui/raw/5ef9e8fc971994cf2bb1d52e956b798c4565f5e9/screenshot.png)


## Software 

This program was made using MATLAB R2021b and Image Processing Toolbox (MATLAB Add-on) version 11.4.

## Usage

Run imPUsingGUI.m file.


## Test Image Sources

 - [@autumnstudio Unsplash](https://unsplash.com/@autumnstudio)
 - [@OliaBondarenko Unsplash](https://unsplash.com/@thdrmdrctr)

